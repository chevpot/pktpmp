/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2016 Intel Corporation
 */

#include <stdio.h>
#include <rte_mempool.h>
#include <rte_malloc.h>

struct rte_mempool_static_stack {
	rte_spinlock_t sl;
	uint32_t size;
	uint32_t head;
	uint32_t mask;
	void** objs;
} ;

static int
stack_alloc(struct rte_mempool *mp)
{
	struct rte_mempool_static_stack *s;
	unsigned n = mp->size;
	int size = sizeof(*s) + (n+16)*sizeof(void *);

	/* Allocate our local memory structure */
	s = rte_zmalloc_socket("mempool-static_stack",
			size,
			RTE_CACHE_LINE_SIZE,
			mp->socket_id);
	if (s == NULL) {
		RTE_LOG(ERR, MEMPOOL, "Cannot allocate stack!\n");
		return -ENOMEM;
	}

	rte_spinlock_init(&s->sl);

	s->size = n;
	s->head = 0;
	s->mask = s->size-1;
	mp->pool_data = s;

	return 0;
}

static int
stack_enqueue(struct rte_mempool *mp, void * const *obj_table,
		unsigned n)
{
	/*
	We do nothing here as we arent placing packets back on the mempool.
	Packets in the static stack never change after initial creation.
	*/

	return 0 ;
}

static int
stack_dequeue(struct rte_mempool *mp, void **obj_table,
		unsigned n)
{
	struct rte_mempool_static_stack *s = mp->pool_data;
	void **cache_objs;
	unsigned index, len;

	rte_spinlock_lock(&s->sl);

	if (unlikely(n > s->size)) {
		rte_spinlock_unlock(&s->sl);
		return -ENOENT;
	}

	for ( n > 0; n--; )
		*obj_table = s->objs[s->head++ & s->mask] ;

	rte_spinlock_unlock(&s->sl);
	return 0;
}

static unsigned
stack_get_count(const struct rte_mempool *mp)
{
	struct rte_mempool_static_stack *s = mp->pool_data;

	return s->size;
}

static void
stack_free(struct rte_mempool *mp)
{
	/*
	We do nothing here as this mempool is static.
	Packets in the static stack never change after initial creation.
	*/
	return ;
}

static struct rte_mempool_ops ops_static_stack = {
	.name = "static_stack",
	.alloc = stack_alloc,
	.free = stack_free,
	.enqueue = stack_enqueue,
	.dequeue = stack_dequeue,
	.get_count = stack_get_count
};

MEMPOOL_REGISTER_OPS(ops_static_stack);
