#include "init.h"

namespace dpdk_wrapper {

    Init::Init() {}
    Init::~Init() {}

    Init::Init(int dummy)
    {
        this->dummy = dummy;
    }

    int Init::Start()
    {
        return this->dummy;
    }
}