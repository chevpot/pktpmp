# distutils: language = c++

from Init cimport Init
cdef class DpdkInit:
    cdef Init*c_init

    def __cinit__(self, int dummy):
        self.c_init = new Init(dummy)

    def __dealloc(self):
        del self.c_init

    def start(self):
        return self.c_init.Start()

    # Attribute access
    @property
    def dummy(self):
        return self.c_init.dummy
    @dummy.setter
    def dummy(self, dummy):
        self.c_init.dummy = dummy
