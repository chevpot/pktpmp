cdef extern from "init.cpp":
    pass

cdef extern from "init.h" namespace "dpdk_wrapper":
    cdef cppclass Init:
        Init() except +
        Init(int) except +
        int dummy
        int Start()