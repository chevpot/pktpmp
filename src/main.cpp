#include "port.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <errno.h>
#include <stdbool.h>

#include <sys/queue.h>
#include <sys/stat.h>

#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>

#include <rte_eal.h>
#include <rte_mempool.h>

struct rte_port *ports;	       /**< For all probed ethernet ports. */


int main(int argc, char** argv)
{
	int diag;
	uint16_t port_id;
	uint16_t count;
	int ret;

	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

    char* eal_argv[32];
	int eal_argc = 0;

	eal_argv[eal_argc++] = (char*) "pktpmp" ;
	eal_argv[eal_argc++] = (char*) "-c" ;

    //TODO Capture coremask dynamically

    uint32_t coreMask = 0;
	for (unsigned coreId = 0; coreId < 4; coreId++)
	{
		coreMask |= (1 << coreId);
	}

    char hexCoreMask[5];
	snprintf(hexCoreMask, 5, "%x", coreMask);

	eal_argv[eal_argc++] = hexCoreMask;
    eal_argv[eal_argc++] = (char*) "-n" ;
	eal_argv[eal_argc++] = (char*) "4" ;

	diag = rte_eal_init(eal_argc, eal_argv);
	if (diag < 0)
    {
		rte_panic("Cannot init EAL\n");
    }

    if (rte_eal_hpet_init(1) < 0)
	{
		rte_panic("Could not init hpet") ;
	}

    // TODO Define and initialise mempools for port handlers and pktpmp pool
    // TODO Define and initialise PortsHandler
    ports = Port[2] ;

	for ( int i = 0, i < 2; i++ )
	{
		if ( !Port[i]->Init(i, i, RX_MASK | TX_MASK) )
		{
			rte_panic("Unable to init port") ;
		}
	}

    // Check core state
    int lcore_id;
	RTE_LCORE_FOREACH_SLAVE(lcore_id)
	{
		if (lcore_config[lcore_id].state != WAIT)
    }

    // TODO Define and initialise pktpmp thread(s)
	for ( int i = 0, i < 2; i++ )
	{
		Port[i]->StartThreads()
	}
		
    // TODO Run main message handler loop, rabbitmq

}
