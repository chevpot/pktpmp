#include <rte_ethdev.h>

#include "port.h"


Port::Port()
{
    nHwPortId = 0 ;
    nLogicalPortId = 0 ;
    nRxTxMask = RX_MASK | TX_MASK ;
    txKill = false ;
    rxKill = false ;
}

Port::~Port()
{

}

bool Port::InitRx(uint32_t hwPortId)
{
    rte_eth_rxconf rxconf ;
    memcpy(&rxconf, &devInfo.default_rxconf, sizeof(rxconf));

    rxconf.rx_drop_en = 0;
    uint16_t rx_descriptors = 2048;

    rte_eth_rx_queue_setup(
        ( uint8_t ) hwPortId,
        0,
        rx_descriptors,
        rte_eth_dev_socket_id(hwPortId),
        &rxconf,
        rxmempool
    ) ;

    std::string ringName = "Port" + std::to_string(hwPortId) + "RxRing" ;

    txRing = rte_ring_create(
        ringName.c_str(),
        2048,
        rte_socket_id(),
        RING_F_SC_DEQ
    ) ;

    return true ;
}

bool Port::InitTx((uint32_t hwPortId)
{
    rte_eth_txconf txconf ;
    memcpy(&txconf, &devInfo.default_txconf, sizeof(txconf));

    txconf.txq_flags = \
        ETH_TXQ_FLAGS_NOMULTSEGS | \
        ETH_TXQ_FLAGS_NOMULTMEMP | \
        ETH_TXQ_FLAGS_NOREFCOUNT | \
		ETH_TXQ_FLAGS_NOVLANOFFL | \
        ETH_TXQ_FLAGS_NOXSUMSCTP | \
		ETH_TXQ_FLAGS_NOXSUMUDP ;

    uint16_t tx_descriptors = 4096 ;

    rte_eth_tx_queue_setup(
        ( uint8_t ) hwPortId,
        0,
        tx_descriptors,
        rte_eth_dev_socket_id(portId),
        &txconf) ;

    std::string ringName = "Port" + std::to_string(hwPortId) + "TxRing" ;

    txRing = rte_ring_create(
        ringName.c_str(),
        2048,
        rte_socket_id(),
        RING_F_SC_DEQ
    ) ;

    return true ;
}

bool Port::Init(uint32_t hwPortId, uint32_t logPortId, uint8_t rxTxMask)
{
    nHwPortId     = hwPortId ;
    nLogicalPorId = logPortId ;
    nRxTxMask     = rxTxMask ;

    if ( rte_eth_dev_is_valid_port(hwPortId) != 1 )
    {
        rte_panic("Invalid Port Id")
    }

    rte_eth_dev_info devInfo;
	memset(&devInfo, 0, sizeof(devInfo));
	rte_eth_dev_info_get(hwPortId, &devInfo);

    struct rte_eth_conf port_conf;
	memset(&port_conf, 0, sizeof(struct rte_eth_conf)) ;

    ret = rte_eth_dev_configure( ( uint8_t ) hwPortId, 1, 1, &port_conf );
	if ( ret < 0 )
	{
        rte_panic("Failed to setup device")
    }

    if ( rxTxMask & RX_MASK )
    {
        std::string poolName = "Port" + std::to_string(i) + "RxMemPool");
        rxmempool = rte_pktmbuf_pool_create_by_ops(
            poolName.c_str(),
            4096,
            256,
            0,
            1500,
            rte_socket_id(),
            "static_stack"
        ) ;
        InitRx(hwPortId) ;
    }

    if ( rxTxMask & TX_MASK )
    {
        InitTx(hwPortId) ;
    }

    rte_eth_promiscuous_enable( ( uint8_t ) hwPortId ) ;
	rte_eth_allmulticast_enable( ( uint8_t ) hwPortId ) ;

    rte_eth_dev_start( ( uint8_t ) hwPortId );

    return true ;
}

bool Port::StartThreads()
{
    if ( rxTxMask & RX_MASK )
    {
        if ( rte_eal_remote_launch(RxThread, (void) this, 0) != 0 )
        {
            rte_panic("Failed to launch rxtx thread")
        }
    }

    if ( rxTxMask & TX_MASK )
    {
        if ( rte_eal_remote_launch(TxThread, (void) this, 0) != 0 )
        {
            rte_panic("Failed to launch rxtx thread")
        }
    }

    return true ;
}

int Port::RxThread(void *args)
{
    rte_mbuf* mbufs[64] ;

    while ( !rxKill )
    {
        deq = rte_eth_rx_burst(nHwPortId, 0, mbufs, 64) ;

        enq = rte_ring_mp_enqueue_bulk(
            rxRing,
            mbufs,
            deq,
            nullptr
        ) ;
    }

    return 0 ;
}

int Port::TxThread(void *args)
{
    rte_mbuf* mbufs[64] ;

    while ( !txKill )
    {
        deq = rte_ring_mc_dequeue_bulk(
            txRing,
            mbufs,
            64,
            nullptr
        ) ;
        enq = rte_eth_tx_burst(
            nHwPortId,
            0,
            mbufs,
            deq
        ) ;
    }

    return 0 ;
}