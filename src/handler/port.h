#pragma once
#include "defines.h"

#include <stdint.h>

#define RX_MASK 0x00000001
#define TX_MASK 0x00000010

class Port
{
    private:
        uint32_t nHwPortId ;
        uint32_t nLogicalPortId ;
        uint8_t  nRxTxMask ;
        rte_ring* rxRing ;
        rte_ring* txRing ;
        rte_mempool* rxmempool ;
        bool txKill ;
        bool rxKill ;
    public:
        Port() ;
        ~Port() ;

        bool Init(uint32_t hwPortId, uint32_t logPortId, uint8_t rxTxMask) ;
        bool InitRx(uint32_t hwPortId) ;
        bool InitTx(uint32_t hwPortId) ;

        static int RxThread(void *args) ;
        static int TxThread(void *args) ;
}