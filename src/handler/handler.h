#include "defines.h"

#include <stdint.h>

class PortsHandler
{
    private:
        uint32_t nPorts[MAX_PORT_COUNT] ;
        uint32_t hwPortCount ;

    public:
        PortsHandler() ;
        ~PortsHandler() ;

        bool Init() ;

} ;