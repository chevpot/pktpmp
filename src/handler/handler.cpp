#include "ports.h"

#include <rte_ethdev.h>

PortsHandler::PortsHandler()
{

}

PortsHandler::~PortsHandler()
{

}

bool PortsHandler::Init()
{
    // Get physical port count
    hwPortCount = rte_eth_dev_count();

    for ( int portId = 0; portId < hwPortCount; i++ )
    {
        InitPort(portId) ;
    }

    return true ;
}

bool PortsHandler::InitPort(uint32_t portId)
{
    if ( rte_eth_dev_is_valid_port(portId) != 1 )
    {
        rte_panic("Invalid Port Id")
    }

    rte_eth_dev_info devInfo;
	memset(&devInfo, 0, sizeof(devInfo));
	rte_eth_dev_info_get(portId, &devInfo);

    struct rte_eth_conf port_conf;
	memset(&port_conf, 0, sizeof(struct rte_eth_conf)) ;

    ret = rte_eth_dev_configure( ( uint8_t ) physicalPortId, 1, 1, &port_conf );
	if (ret < 0)
	{
        rte_panic("Failed to setup device")
    }

    rte_eth_rxconf rxconf ;
	rte_eth_txconf txconf ;
	memcpy(&rxconf, &devInfo.default_rxconf, sizeof(rxconf));
	memcpy(&txconf, &devInfo.default_txconf, sizeof(txconf));

    rxconf.rx_drop_en = 0;

    txconf.txq_flags = \
        ETH_TXQ_FLAGS_NOMULTSEGS | \
        ETH_TXQ_FLAGS_NOMULTMEMP | \
        ETH_TXQ_FLAGS_NOREFCOUNT | \
		ETH_TXQ_FLAGS_NOVLANOFFL | \
        ETH_TXQ_FLAGS_NOXSUMSCTP | \
		ETH_TXQ_FLAGS_NOXSUMUDP ;

    uint16_t rx_descriptors = 2048;
	uint16_t tx_descriptors = 4096;

    // rte_eth_rx_queue_setup( ( uint8_t ) portId, 0, rx_descriptors, rte_eth_dev_socket_id(portId), &rxconf, portsMemPool);
    // rte_eth_tx_queue_setup( ( uint8_t ) portId, 0, tx_descriptors, rte_eth_dev_socket_id(portId), &txconf);

    rte_eth_promiscuous_enable( ( uint8_t ) portId );
	rte_eth_allmulticast_enable( ( uint8_t ) portId );

	struct rte_eth_fc_conf fc_conf;
	memset(&fc_conf, 0, sizeof(fc_conf));
	fc_conf.mode       = RTE_FC_NONE ;
	fc_conf.high_water = 0 ;
	fc_conf.low_water  = 0 ;
	fc_conf.pause_time = 1 ;
	fc_conf.send_xon   = 0 ;

    rte_eth_dev_start( ( uint8_t ) portId );

    return ;
}