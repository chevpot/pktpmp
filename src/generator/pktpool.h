#pragma once

#include <rte_mbuf.h>
#include "ring.h"

class PktPool : public Ring
{
	private:
		struct rte_mempool * pktpool ;
	public:
		PktPool();
		virtual ~PktPool();

		bool Init(uint32_t maxPackets, int socket) ;
		bool Enqueue(rte_mbuf * packet) ;
		uint32_t Dequeue(rte_mbuf * packets[], uint32_t maxPacketCount) ;
		uint32_t Enqueue(rte_mbuf * packets[], uint32_t packetCount) ;
		void CreateStaticPacketPool() ;
		rte_mbuf* PeekHead( ) ;
		rte_mbuf* PeekTail( ) ;
		void Close() ;

		// rte_mbuf ** entries;
} ;
