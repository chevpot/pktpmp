#include "pktpool.h"
#include <rte_malloc.h>


PktPool::PktPool() : Ring()
{
	entries = NULL ;
}

PktPool::~PktPool()
{
	Close() ;
}

void PktPool::CreateStaticPacketPool()
{
	std::string poolName = "StaticPktPool";
	pktpool = rte_pktmbuf_pool_create_by_ops(
		poolName.c_str(),
		4096,
		256,
		0,
		1500,
		rte_socket_id(),
		"static_stack"
	)

	return ;
}

bool_t PktPool::Init(uint32_t maxPackets, int socket)
{
	std::string poolName = "StaticPktPool";

	pktpool = rte_pktmbuf_pool_create_by_ops(
		poolName.c_str(),
		4096,
		256,
		0,
		1500,
		rte_socket_id(),
		"static_stack"
	) ;

	return true ;

	// if (PktPool::Init(maxPackets) == FALSE)
	// 	return false ;

	// entries = (rte_mbuf **)rte_zmalloc_socket(NULL, maxPackets * sizeof(rte_mbuf *), 0, socket);
	// if (entries == NULL)
	// 	return false ;

	// return true ;
}

bool_t PktPool::Enqueue(rte_mbuf * packet)
{
	if (GetFree() == 0)
		return false ;

	entries[(head++) & mask] = packet ;
	return true ;
}

uint32_t PktPool::Enqueue(rte_mbuf * packets[], uint32_t packetCount)
{
	uint32_t freeCount = GetFree() ;
	if (freeCount < packetCount)
		packetCount = freeCount ;

	for (uint32_t i = 0; i < packetCount; i++)
	{
		entries[(head++) & mask] = packets[i] ;
	}

	return packetCount;
}

uint32_t PktPool::Dequeue(rte_mbuf *packets[], uint32_t maxPacketCount)
{
	uint32_t dequeued = 0 ;

	while (dequeued < maxPacketCount && tail != head)
	{
		packets[dequeued++] = entries[(tail++) & mask] ;
	}

	return dequeued ;
}

rte_mbuf* PktPool::PeekHead( )
{
	return entries[(head & mask)] ;
}

rte_mbuf* PktPool::PeekTail( )
{
	return entries[(tail & mask)] ;
}

void PktPool::Close()
{
	if (entries != nullptr)
	{
		rte_free(entries) ;
		entries = nullptr ;
	}
}
