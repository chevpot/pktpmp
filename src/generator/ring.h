#pragma once
#include <stdint.h>

#include "defines.h"


class Ring
{
    private:
        uint64_t head ;
        uint64_t tail ;
        uint64_t size ;
        uint64_t mask ; 

    public:
        Ring()
        {
            uint64_t head = 0 ;
            uint64_t tail = 0 ;
            uint64_t size = 0 ;
            uint64_t mask = 0 ;
        }

        virtual ~Ring() {} ;

        bool Init(uint64_t size)
        {
            if( ( (size-1) & size ) != 0 )
            {
			    return false;
            }

            if ( size < 4 )
            {
                return false ;
            }

            head = 0;
            tail = 0;
            size = size;
            mask = size - 1;

            return true ;
        }

        inline void Reset()
        {
            head = 0;
            tail = 0;
        }

        inline uint64_t GetFree()
        {
            return size - GetUsed();
        }

        inline uint64_t GetUsed()
        {
            return (head - tail);
        }

        inline uint64_t GetCurrentHeadIndex()
        {
            return (head & mask) ;
        }

        inline uint64_t GetCurrentTailIndex()
        {
            return (tail & mask) ;
        }

        inline uint64_t GetNextHeadIndex()
        {
            return ((head + 1) & mask);
        }

        inline uint64_t GetNextTailIndex()
        {
            return ((tail + 1) & mask);
        }


}